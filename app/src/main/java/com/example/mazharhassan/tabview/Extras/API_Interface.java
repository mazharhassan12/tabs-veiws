package com.example.mazharhassan.tabview.Extras;

import android.telecom.Call;

import retrofit2.http.GET;

/**
 * Created by mazharhassan on 8/15/2017.
 */

public interface API_Interface {

    @GET("ayoub_js_auto_vehicles_json.php")
    retrofit2.Call<Auto_Vehicle> ayoub_js_auto_vehicles_json();
}
