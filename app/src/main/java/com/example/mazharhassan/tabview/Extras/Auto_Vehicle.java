package com.example.mazharhassan.tabview.Extras;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mazharhassan on 8/15/2017.
 */

public class Auto_Vehicle implements Serializable {

    @SerializedName("ayoub_js_auto_vehicles_pf")
    @Expose
    private AyoubJsAutoVehiclesPf ayoubJsAutoVehiclesPf;

    public AyoubJsAutoVehiclesPf getAyoubJsAutoVehiclesPf() {
        return ayoubJsAutoVehiclesPf;
    }

    public void setAyoubJsAutoVehiclesPf(AyoubJsAutoVehiclesPf ayoubJsAutoVehiclesPf) {
        this.ayoubJsAutoVehiclesPf = ayoubJsAutoVehiclesPf;
    }


    public class AyoubJsAutoVehiclesFeatured implements Serializable {

        @SerializedName("fmake_title")
        @Expose
        private String fmakeTitle;
        @SerializedName("fmodel_title")
        @Expose
        private String fmodelTitle;
        @SerializedName("fmodel_year_title")
        @Expose
        private Integer fmodelYearTitle;
        @SerializedName("fvehicle_price")
        @Expose
        private String fvehiclePrice;
        @SerializedName("fvehicle_description")
        @Expose
        private String fvehicleDescription;

        @SerializedName("vehicle_id")
        @Expose
        private String vehicle_id;

        public String getVehicle_id() {
            return getVehicle_id();
        }

        public String getFmakeTitle() {
            return fmakeTitle;
        }

        public void setFmakeTitle(String fmakeTitle) {
            this.fmakeTitle = fmakeTitle;
        }

        public String getFmodelTitle() {
            return fmodelTitle;
        }

        public void setFmodelTitle(String fmodelTitle) {
            this.fmodelTitle = fmodelTitle;
        }

        public Integer getFmodelYearTitle() {
            return fmodelYearTitle;
        }

        public void setFmodelYearTitle(Integer fmodelYearTitle) {
            this.fmodelYearTitle = fmodelYearTitle;
        }

        public String getFvehiclePrice() {
            return fvehiclePrice;
        }

        public void setFvehiclePrice(String fvehiclePrice) {
            this.fvehiclePrice = fvehiclePrice;
        }

        public String getFvehicleDescription() {
            return fvehicleDescription;
        }

        public void setFvehicleDescription(String fvehicleDescription) {
            this.fvehicleDescription = fvehicleDescription;
        }

    }

    public class AyoubJsAutoVehiclesPf implements Serializable {

        @SerializedName("ayoub_js_auto_vehicles_premium")
        @Expose
        private List<AyoubJsAutoVehiclesPremium> ayoubJsAutoVehiclesPremium = null;
        @SerializedName("ayoub_js_auto_vehicles_featured")
        @Expose
        private List<AyoubJsAutoVehiclesPremium> ayoubJsAutoVehiclesFeatured = null;


        public List<AyoubJsAutoVehiclesPremium> getAyoubJsAutoVehiclesPremium() {
            return ayoubJsAutoVehiclesPremium;
        }

        public void setAyoubJsAutoVehiclesPremium(List<AyoubJsAutoVehiclesPremium> ayoubJsAutoVehiclesPremium) {
            this.ayoubJsAutoVehiclesPremium = ayoubJsAutoVehiclesPremium;
        }

        public List<AyoubJsAutoVehiclesPremium> getAyoubJsAutoVehiclesFeatured() {
            return ayoubJsAutoVehiclesFeatured;
        }

        public void setAyoubJsAutoVehiclesFeatured(List<AyoubJsAutoVehiclesPremium> ayoubJsAutoVehiclesFeatured) {
            this.ayoubJsAutoVehiclesFeatured = ayoubJsAutoVehiclesFeatured;
        }

    }

    public class AyoubJsAutoVehiclesPremium implements Serializable {

        @SerializedName("vehicle_id")
        @Expose
        private String vehicle_id;

        public String getVehicle_id() {
            return getVehicle_id();
        }

        @SerializedName("make_title")
        @Expose
        private String makeTitle;
        @SerializedName("model_title")
        @Expose
        private String modelTitle;
        @SerializedName("model_year_title")
        @Expose
        private Integer modelYearTitle;
        @SerializedName("vehicle_price")
        @Expose
        private String vehiclePrice;
        @SerializedName("vehicle_description")
        @Expose
        private String vehicleDescription;

        public String getMakeTitle() {
            return makeTitle;
        }

        public void setMakeTitle(String makeTitle) {
            this.makeTitle = makeTitle;
        }

        public String getModelTitle() {
            return modelTitle;
        }

        public void setModelTitle(String modelTitle) {
            this.modelTitle = modelTitle;
        }

        public Integer getModelYearTitle() {
            return modelYearTitle;
        }

        public void setModelYearTitle(Integer modelYearTitle) {
            this.modelYearTitle = modelYearTitle;
        }

        public String getVehiclePrice() {
            return vehiclePrice;
        }

        public void setVehiclePrice(String vehiclePrice) {
            this.vehiclePrice = vehiclePrice;
        }

        public String getVehicleDescription() {
            return vehicleDescription;
        }

        public void setVehicleDescription(String vehicleDescription) {
            this.vehicleDescription = vehicleDescription;
        }

    }

}

