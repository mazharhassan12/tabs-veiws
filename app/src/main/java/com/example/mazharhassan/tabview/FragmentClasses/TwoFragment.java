package com.example.mazharhassan.tabview.FragmentClasses;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.mazharhassan.tabview.Adapters.PremiumAdapter;
import com.example.mazharhassan.tabview.Extras.API_Client;
import com.example.mazharhassan.tabview.Extras.API_Interface;
import com.example.mazharhassan.tabview.Extras.Auto_Vehicle;
import com.example.mazharhassan.tabview.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TwoFragment extends Fragment {

    private RecyclerView recyclerViewFeature;
    private RecyclerView.LayoutManager layoutManager;
    private PremiumAdapter adapter;
    private API_Interface api_interface;

    public TwoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        API_Interface apiService = API_Client.getClient().create(API_Interface.class);


        Call<Auto_Vehicle> call = apiService.ayoub_js_auto_vehicles_json();
        call.enqueue(new Callback<Auto_Vehicle>() {
            @Override
            public void onResponse(Call<Auto_Vehicle> call, Response<Auto_Vehicle> response) {

                Auto_Vehicle av = response.body();

                List<Auto_Vehicle.AyoubJsAutoVehiclesPremium> f = av.getAyoubJsAutoVehiclesPf().getAyoubJsAutoVehiclesFeatured();

                adapter = new PremiumAdapter(f);

                LinearLayoutManager llm = new LinearLayoutManager(getContext());
                llm.setOrientation(LinearLayoutManager.VERTICAL);
                recyclerViewFeature.setLayoutManager(llm);

                recyclerViewFeature.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Auto_Vehicle> call, Throwable t) {

            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View rootView = inflater.inflate(R.layout.fragment_two, container,
                false);

        recyclerViewFeature = (RecyclerView)rootView.findViewById(R.id.recyclerViewPremium);
        recyclerViewFeature.setHasFixedSize(true);
        return rootView;
    }
}
