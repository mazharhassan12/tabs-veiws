package com.example.mazharhassan.tabview.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mazharhassan.tabview.Extras.Auto_Vehicle;
import com.example.mazharhassan.tabview.R;

import java.util.List;

import static com.example.mazharhassan.tabview.R.id.imageView;

/**
 * Created by mazharhassan on 8/15/2017.
 */

public class PremiumAdapter extends RecyclerView.Adapter<PremiumAdapter.MyViewHolder> {
    private List<Auto_Vehicle.AyoubJsAutoVehiclesPremium> p;
    //Context context;

    public PremiumAdapter(List<Auto_Vehicle.AyoubJsAutoVehiclesPremium> p) {
        this.p = p;
        //this.context = context;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.design,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

       // holder.id.setText(p.get(position).getVehicle_id().toString());
        holder.name.setText(p.get(position).getMakeTitle().toString());
        holder.model.setText(p.get(position).getModelTitle().toString());
        holder.year.setText(p.get(position).getModelYearTitle().toString());
        holder.price.setText(p.get(position).getVehiclePrice().toString());
        holder.description.setText(p.get(position).getVehicleDescription().toString());
        //Picasso.with(holder.img.getContext()).load("https://bongocars.com/js_autoz_data/data/vehicle/vehicle_24/images/jsauto_201_21948210001_large.jpg").into(holder.img);

        /*Picasso.with(holder.itemView.getContext()).load(al_prod_image.get(position))
                .placeholder(R.drawable.floatingicon)
                .error(R.drawable.floatingicon).
                   .into(viewHolder.img_prod);*/

    }

    @Override
    public int getItemCount() {
        return p.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView name,model,year,price,description,id;
        ImageView img;

        public MyViewHolder(View itemView) {
            super(itemView);


            name = (TextView) itemView.findViewById(R.id.make_title);
            model = (TextView) itemView.findViewById(R.id.model_title);
            year = (TextView) itemView.findViewById(R.id.year_title);
            price = (TextView) itemView.findViewById(R.id.vehicle_price);
            description = (TextView) itemView.findViewById(R.id.description);
            img = (ImageView) itemView.findViewById(imageView);



        }
    }
}
