package com.example.mazharhassan.tabview.Extras;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mazharhassan on 8/21/2017.
 */

public class Sellers implements Serializable
{

    @SerializedName("ayoub_js_auto_seller_contact_info")
    @Expose

    private List<AyoubJsAutoSellerContactInfo> ayoubJsAutoSellerContactInfo = null;

    public List<AyoubJsAutoSellerContactInfo> getAyoubJsAutoSellerContactInfo() {
        return ayoubJsAutoSellerContactInfo;
    }

    public void setAyoubJsAutoSellerContactInfo(List<AyoubJsAutoSellerContactInfo> ayoubJsAutoSellerContactInfo) {
        this.ayoubJsAutoSellerContactInfo = ayoubJsAutoSellerContactInfo;
    }


    public class AyoubJsAutoSellerContactInfo implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("uid")
        @Expose
        private String uid;
        @SerializedName("vehicleid")
        @Expose
        private String vehicleid;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("cell")
        @Expose
        private String cell;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("weblink")
        @Expose
        private String weblink;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created")
        @Expose
        private String created;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getUid() {
            return uid;
        }

        public void setUid(String uid) {
            this.uid = uid;
        }

        public String getVehicleid() {
            return vehicleid;
        }

        public void setVehicleid(String vehicleid) {
            this.vehicleid = vehicleid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCell() {
            return cell;
        }

        public void setCell(String cell) {
            this.cell = cell;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getWeblink() {
            return weblink;
        }

        public void setWeblink(String weblink) {
            this.weblink = weblink;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreated() {
            return created;
        }

        public void setCreated(String created) {
            this.created = created;
        }
    }

}
